import React, { useState } from 'react';
import ReactDOM from 'react-dom/client';

const History = (props) => {
  if (props.allClicks.length === 0) {
    return (
      <div>
        the app is used by pressing the buttons
      </div>
    );
  }

  return (
    <div>
      Pressed Clicks: {props.allClicks.join(' ')}
    </div>
  )
}

const Button = ({ onClick, text }) =>
  <button onClick={onClick}>{text}</button>;

// The `useState` and `useEffect` hooks can only be used into a function that
// define a component, you can't use it in a loop, a conditional statement or
// another statement that is not a functional component.
// If you do that, the app will start acting erratic.
const App = () => {
  const [left, setLeft] = useState(0);
  const [right, setRight] = useState(0);
  const [allClicks, setAll] = useState([]);

  const handleLeftClick = () => {
    setAll(allClicks.concat('L'));
    setLeft(left + 1);
  }

  const handleRightClick = () => {
    setAll(allClicks.concat('R'));
    setRight(right + 1);
  }

  return (
    <>
      <div>
        {left}
        <Button onClick={handleLeftClick} text="left" />
        <Button onClick={handleRightClick} text="right" />
        {right}
        <History allClicks={allClicks} />
      </div>
    </>
  );
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
